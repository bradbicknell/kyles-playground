### Setup

* Install node/npm

Ubuntu
```
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Mac OSX
```
brew install node
```
* Run `npm install` from this folder`

* Install http-server `sudo npm install http-server -g`


### Development

* Run `gulp watch`

* Run `http-server` from this folder

* Visit localhost:8080

* Change files in sass/ and watch things magically take affect!
