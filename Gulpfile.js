var gulp = require('gulp');
var sass = require('gulp-sass');
var watch    = require('gulp-watch');
var concat   = require('gulp-concat');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var liveReload   = require("gulp-livereload");


function compilesass() {
    var srcPath  = "./sass/application.scss",
        destPath = "./public/";

    var result = gulp.src(srcPath)
        .pipe(sourcemaps.init({loadMaps: true})) // Tell gulp we want sourcemaps too
        .pipe(sass({imagePath: '/assets/images'}).on('error', sass.logError))
        .pipe(concat('application.css'))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({ browsers: ['last 2 version'] })) // Use browser specific prefixes
        .pipe(gulp.dest(destPath));

    // Since we still use the asset pipeline we need to say that this file changed so livereload works correctly
    liveReload.changed('./public/application.css');

    return result;
}

gulp.task('sass', function () { return compilesass() });
gulp.task('watch', function(callback) {
    liveReload.listen();

    watch(['./sass/**/*'], function() { gulp.start('sass') });
});
gulp.task('default', ['sass']);
